import CareScale from "../../utils/CareScale";
import "../../styles/PlantItem.css"

const PlantItem = ({ id, cover, plant_name, water, light }) =>
{
    return (
        <li key={id} className='lmj-plant-item'>
            <img className='lmj-plant-item-cover' src={cover} alt={`${plant_name} cover`} />
            {plant_name}
            <div>
                <CareScale careType='water' scaleValue={water} />
                <CareScale careType='light' scaleValue={light} />
            </div>
        </li>
    )
}

export default PlantItem;