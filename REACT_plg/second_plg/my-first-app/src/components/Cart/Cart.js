import "../../styles/Cart.css";
import { GetTotalCartValue } from "../../utils/loop";

const Cart = () =>
{
        const plants = [
                { name: "MONSTERA", price: 4 },
                { name: "LIERRE", price: 7 },
                { name: "FLOWERS POTS", price: 12 }
        ];
        let total;
        return (
                <div>
                        <ul>{plants.map((plant, index) => (
                                <li key={index}>{plant.name}: {plant.price} €</li>
                        ))}</ul>
                        <div className="total_cart">TOTAL DE MON PANIER: {GetTotalCartValue(plants, total)} €</div>
                </div>)
}

export default Cart;