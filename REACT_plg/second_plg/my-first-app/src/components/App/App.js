import '../../styles/App.css';
import Banner from '../Banner/Banner';
import Cart from '../Cart/Cart';
import ShoppinList from '../PlantList/ShoppinList';

function App()
{
  return (<div className="App">
    <Banner />
    <ShoppinList />
  </div>
  );
}

export default App;