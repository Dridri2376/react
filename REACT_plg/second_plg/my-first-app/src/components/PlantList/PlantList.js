import monstera from '../../assets/monstera.jpg'

export const PlantList = [
    {
        plant_name: 'monstera',
        category: 'classique',
        id: '1ed',
        isGoodSale: true,
        light: 3,
        water: 1,
        cover: monstera
    },
    {
        plant_name: 'ficus lyrata',
        category: 'classique',
        id: '2ab',
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'pothos argenté',
        category: 'classique',
        id: '2sd',
        isGoodSale: true,
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'yucca',
        category: 'classique',
        id: '4kk',
        light: 3,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'olivier',
        category: 'extérieur',
        id: '5pl',
        isGoodSale: true,
        light: 1,
        water: 2,
        cover: monstera
    },
    {
        plant_name: 'géranium',
        category: 'extérieur',
        id: '6uo',
        isGoodSale: true,
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'basilique',
        category: 'extérieur',
        id: '7ie',
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'aloe',
        category: 'plante grasse',
        id: '8fp',
        isGoodSale: true,
        light: 2,
        water: 2,
        cover: monstera
    },
    {
        plant_name: 'succulente',
        category: 'plante grasse',
        id: '9vn',
        isGoodSale: true,
        light: 1,
        water: 2,
        cover: monstera
    },
    {
        plant_name: 'monstera',
        category: 'classique',
        id: '1ed',
        isGoodSale: true,
        light: 3,
        water: 1,
        cover: monstera
    },
    {
        plant_name: 'ficus lyrata',
        category: 'classique',
        id: '2ab',
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'pothos argenté',
        category: 'classique',
        id: '2sd',
        isGoodSale: true,
        light: 2,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'yucca',
        category: 'classique',
        id: '4kk',
        light: 3,
        water: 3,
        cover: monstera
    },
    {
        plant_name: 'olivier',
        category: 'extérieur',
        id: '5pl',
        isGoodSale: true,
        light: 1,
        water: 2,
        cover: monstera
    }
]

