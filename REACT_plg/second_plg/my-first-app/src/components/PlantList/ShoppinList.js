import { PlantList } from "./PlantList";
import "../../styles/ShoppinList.css";
import PlantItem from "../PlantItem/PlantItem";
import React from "react";


export default class ShoppinList extends React.Component
{
    uniqueCategories = [];
    category = "";

    getUniquesCategories()
    {
        this.uniqueCategories = PlantList.reduce((newArray, plant) =>
        {
            return newArray.includes(plant.category) /*false a la base*/ ?
                newArray /* si yavait deja la category, il renvoit l'array */ :
                newArray.concat(plant.category) /* sinon il concat (l'ajoute)*/
        }, [] // => cette merde veut dire que, en gros, il utilise ca pour savoir ce qu'est "l'accumulateur" (first arg) du reduce();
            // => la, comme on afficher des données, on, doit donc itérer sur un tableau (comme vue ou angular), DONC on lui dit que l'accumulateur est un []
        );

        return (
            <ul>
                {this.uniqueCategories.map((category, index) =>
                    <li key={index}>{category}</li>
                )
                }
            </ul>)
    };

    /* getPlantList()
    {
        {
            return (
                <ul>
                    {
                        PlantList.map((plant) => <li key={plant.id}>{plant.name},
                            <div>
                                <CareScale typeDeSoin="light" NoteAttribueAuSoin={plant.light} />
                                <CareScale typeDeSoin="water" NoteAttribueAuSoin={plant.water} />
                            </div>
                        </li>)
                    }
                </ul>
            )
        };
    } */

    render()
    {
        return (<div>
            <ul>
                {/* elle veut un <PlantItem /> avec un enfant <CareScale /> */}
                {this.getUniquesCategories()}
            </ul>
            {/* {this.getPlantList()} */}
            <ul className="lmj-plant-list">{PlantList.map(({ id, cover, plant_name, water, light }) =>
            {
                return (<PlantItem
                    id={id}
                    cover={cover}
                    plant_name={plant_name}
                    water={water}
                    light={light}
                />
                )
            })}
            </ul>
        </div>
        )
    };
}
