import "../../styles/Banner.css";
import logo from "../../assets/logo.png";
import Recommendations from '../../components/Recommendations/Recommendations';
const Banner = () =>
{
    const mainTitle = "la maison jungle";

    return (
        <div className="lmj-banner">
            <img src={logo} alt="une feuille" />
            <h1>{mainTitle}</h1>
            <div className="reco">
                <Recommendations />
            </div>

        </div>
    )
}

export default Banner;