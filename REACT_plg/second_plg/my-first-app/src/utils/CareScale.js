const CareScale = ({ NoteAttribueAuSoin: NoteAttribueAuSoin, typeDeSoin: typeDeSoin }) =>
{
    const range = [1, 2, 3];
    const infosClient = typeDeSoin == 'light' ? '🌞' : '💧';

    return (<div>
        {range.map((number) =>
            NoteAttribueAuSoin >= number ?
                <span key={number}>{infosClient}</span> :
                null
        )
        }
    </div>);
}


// C censé etre ca une " props";
export default CareScale;