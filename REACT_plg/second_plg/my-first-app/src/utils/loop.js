export const GetTotalCartValue = (array, receiver) =>
{
    receiver = 0;
    for (let index = 0; index < array.length; index++)
    {
        const element = array[index].price;
        receiver = receiver + element;
    }
    return receiver;
};

/* const gateau = (array) =>
{
    let total = 0;
    array.reduce((acc, val) => { return total = acc += val }, [])
    return total;
}

console.log(gateau([1, 2, 5])); */
